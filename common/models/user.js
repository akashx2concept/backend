'use strict';
var moment = require('moment-timezone');

let sendingMailService = require('../../server/api/mail/sendingMail');

module.exports = function(User) {

    User.validatesInclusionOf('role', {in: ['Root', 'User', 'Vendor']});


    // user.validatesPresenceOf('name', 'email');
    // user.validatesLengthOf('password', {min: 5, message: {min: 'Password is too short'}});
    // user.validatesInclusionOf('gender', {in: ['male', 'female']});
    // user.validatesExclusionOf('domain', {in: ['www', 'billing', 'admin']});
    // user.validatesNumericalityOf('age', {int: true});
    // user.validatesUniquenessOf('email', {message: 'email is not unique'});

    User.observe('before save', async (ctx, next) => {
        if (ctx.instance && !ctx.instance.id) {
          ctx.instance.createdAt = moment().format('x');
          let userData = await User.findOne({where: { email: ctx.instance.email }});
          if (!userData) {  
              ctx.instance.isEmailVerified = false;
              setTimeout(() => {
                User.app.models.StripePayment.createCustomerStripe(ctx.instance['id']);
                method_For_sending_emailVerification(ctx.instance);
              }, 5000); 
          }     
        }
        if (ctx && ctx.data) {
          ctx.data.updatedAt = moment().format('x');
          delete ctx.data.isEmailVerified;
        }
        next();
    });

    
    User.observe('after save', async (ctx, next) => {
      
      if (ctx.instance && ctx.instance.id) { 
        // console.log("$$$$$$$$$$$$$$$$$$$$")
        let roleRepository = User.app.models.role;
        let roleMappingRepository = User.app.models.roleMapping;
        // console.log(ctx.instance, " -- instance");

        let userData = await User.findOne({where: {id: ctx.instance['id'] }});
        let roleMappingData = await roleMappingRepository.findOne({where: { principalId: userData['id'] }});

        if (roleMappingData) {
          let roleData = await roleRepository.findOne({ where: { name: userData['role'] } });
          // console.log(roleData, " -- roleData update");
          await roleMappingRepository.upsert({ id: roleMappingData['id'], principalId: userData['id'], principalType: User.app.models.RoleMapping.USER, roleId: roleData['id']});
        } else {
          let roleData = await roleRepository.findOne({ where: { name: userData['role'] } });
          // console.log(roleData, " -- roleData create");
          await roleMappingRepository.create({ principalId: userData['id'], principalType: User.app.models.RoleMapping.USER, roleId: roleData['id'] });
        }

      }
    })


    async function method_For_sending_emailVerification (userData) {
      let userAuthCodeRepository = User.app.models.UserAuthCode;
      let createToken = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 55) + Math.random().toString(36).substring(2, 55) + Math.random().toString(36).substring(2, 55) + Math.random().toString(36).substring(2, 55);
      let data = {
        email: userData.email,
        userName: `${userData.firstName} ${userData.lastName}`,
        subject: 'Verification Mail',
        mailType: 'VerificationMail',
        createToken: createToken
      }
      await userAuthCodeRepository.create({ token: createToken, userId: userData['id'] })
      await sendingMailService.sendMail_usingSendgrid (data);
    }


    User.afterRemote('logout', function (ctx, result, next) {
      let AccessToken = User.app.models.AccessToken;
      AccessToken.destroyById(ctx.req.accessToken.id, function (err, res) {
          if (err) console.log(err);
      });
      let res = ctx.res;
      return next();
    });


    User.afterRemote('login', async function (ctx, result, next) {
      let userAuthCodeRepository = User.app.models.UserAuthCode;
      if (result && result['userId']) {
        // let userData =  await User.findOne({where: { id: result['userId'] }});
        // var code = Math.floor(1000 + Math.random() * 9000);
        // let data = {
        //   email: userData.email,
        //   userName: `${userData.firstName} ${userData.lastName}`,
        //   subject: 'Login OTP',
        //   mailType: '2WayAuthenticationMail',
        //   code: code
        // }
        // await userAuthCodeRepository.create({ code: code, token: result['id'], userId: userData['id'] })
        // await sendingMailService.sendMail_usingSendgrid (data);
      }
      return next();
    });


    User.emailOTPVerification = async function(ctx, options, cb) {
      let requestBody = ctx.req.body; 
      if (requestBody['token'] && requestBody['code']) {
        let userAuthCodeRepository = User.app.models.UserAuthCode;
        let userAuthCodeData = await userAuthCodeRepository.findOne({ where: { token: requestBody['token'] } });
        if (userAuthCodeData) {
          let validCode = await userAuthCodeRepository.findOne({ where: { token: requestBody['token'], code: requestBody['code'] } });
          if (validCode) {
            await userAuthCodeRepository.destroyById(validCode['id']);
            ctx.res.status(200).send( { massage: 'OTP is been verified' })
          } 
          else { ctx.res.status(422).send(new Error("Code is Invalid or expire")) }
        } 
        else { ctx.res.status(422).send(new Error("Invalid token")) }
      } 
      else { ctx.res.status(422).send(new Error("Required Filed Missing 'token' & 'code")) }
    };



    User.sendEmailAgainVerification = async (ctx, options, cb) => {
      let requestBody = ctx.req.body;     
      if (requestBody['email']) {
        let userData = await User.findOne({ where: { email: requestBody['email'] } });
        if (userData) {
          if (!userData['isEmailVerified']) {
            let userAuthCodeRepository = User.app.models.UserAuthCode;
            let createToken = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 55) + Math.random().toString(36).substring(2, 55) + Math.random().toString(36).substring(2, 55) + Math.random().toString(36).substring(2, 55);
            let data = {
              email: userData.email,
              userName: `${userData.firstName} ${userData.lastName}`,
              subject: 'Verification Mail',
              mailType: 'VerificationMail',
              createToken: createToken
            }
            await userAuthCodeRepository.create({ token: createToken, userId: userData['id'] })
            await sendingMailService.sendMail_usingSendgrid (data);
            ctx.res.status(200).send( { massage: 'Check your mail to verify your mailId' });
          } 
          else { ctx.res.status(200).send( { massage: 'Email is already verify' }); }
        } 
        else { ctx.res.status(422).send(new Error("Invalid User Email")); }
      } 
      else {  ctx.res.status(422).send(new Error("Required Filed Missing 'email'")); cb(new Error("Required Filed Missing 'email'"))}
    };


    User.emailVerification = async (ctx, options, cb) => {
      let requestBody = ctx.req.body;
      let userAuthCodeRepository = User.app.models.UserAuthCode;
      let data = await userAuthCodeRepository.findOne({where: { token: requestBody['token'] }, include: [ {relation: 'user'} ]});
      if (data && data['token'] && data['userId']) {
        await User.update({ id: data['userId'], isEmailVerified: true })
        await userAuthCodeRepository.destroyById(data['id'])
        ctx.res.status(200).send({ massage: "EmailId is verified successfully" });
      } else {
        ctx.res.status(422).send(new Error('Token is expired or invalid'));
      }
    };   


    User.vendorProductDashboardGraph = async (ctx, options, cb) => { 
      let requestBody = ctx.req.body;

      // var fullDataForGraph = Object.assign([], filterData);
      var lineProductLabels = [];
      var lineVendorData = [];
      var lineChartLabels = [];

      let productsRepository = User.app.models.Products;
      let vendorsRepository = User.app.models.Vendors;

      for (let i = 11; i >= 0; i--) {
        if (i == 0) {
          lineChartLabels.push(moment().format("MMM YYYY"));
          var month_dayStart = moment().startOf('month').format('x');
          var month_dayEnd = moment().endOf('day').format("x");
          // var query = Object.assign({}, forGraphFilter);
          // query['where']['createdAt'] = {
          //   between: [month_dayStart, month_dayEnd]
          // };
          let query = { where: { createdAt: { between: [ month_dayStart, month_dayEnd ] }} };
          var productList = await productsRepository.find(query);
          var vendorsList = await vendorsRepository.find(query);
          lineProductLabels.push(productList.length);
          lineVendorData.push(vendorsList.length);
        } else {
          lineChartLabels.push(moment().subtract(i, 'month').format("MMM YYYY"));
          var month_dayStart = moment(moment().subtract(i, 'month')).startOf('month').format("x");
          var month_dayEnd = moment(moment().subtract(i, 'month')).endOf('month').format("x");
          // var query = Object.assign({}, forGraphFilter);
          // query['where']['createdAt'] = {
          //   between: [month_dayStart, month_dayEnd]
          // };
          let query = { where: { createdAt: { between: [ month_dayStart, month_dayEnd ] }} };
          var productList = await productsRepository.find(query);
          var vendorsList = await vendorsRepository.find(query);
          lineProductLabels.push(productList.length);
          lineVendorData.push(vendorsList.length);
        }
      }

      let graphData = {
        lineProductLabels: lineProductLabels,
        lineVendorData: lineVendorData,
        lineChartLabels: lineChartLabels
      }
      ctx.res.status(200).send(graphData);
    }


    User.orderSalesDashboardGraph = async (ctx, options, cb) => {
      var lineTotalOrderPlaced = [];
      var lineTotalDeliveredOrder = [];
      var lineTotalCancelOrder = [];
      var lineChartLabels = [];

      let ordersRepository = User.app.models.Orders;

      for (let i = 11; i >= 0; i--) {
        if (i == 0) {
          lineChartLabels.push(moment().format("MMM YYYY"));
          var month_dayStart = moment().startOf('month').format('x');
          var month_dayEnd = moment().endOf('day').format("x");
          let lineTotalOrderPlaced_query = { where: { status: "OrderPlace", createdAt: { between: [ month_dayStart, month_dayEnd ] }} };
          let lineTotalDeliveredOrder_query = { where: { deliveryStatus: "Delivered", createdAt: { between: [ month_dayStart, month_dayEnd ] }} };
          let lineTotalCancelOrder_query = { where: { deliveryStatus: "Cancelled", createdAt: { between: [ month_dayStart, month_dayEnd ] }} };
          var lineTotalOrderPlaced_list = await ordersRepository.find(lineTotalOrderPlaced_query);
          var lineTotalDeliveredOrder_list = await ordersRepository.find(lineTotalDeliveredOrder_query);
          var lineTotalCancelOrder_list = await ordersRepository.find(lineTotalCancelOrder_query);
          lineTotalOrderPlaced.push(lineTotalOrderPlaced_list.length);
          lineTotalDeliveredOrder.push(lineTotalDeliveredOrder_list.length);
          lineTotalCancelOrder.push(lineTotalCancelOrder_list.length);
        }
        else { 
          lineChartLabels.push(moment().subtract(i, 'month').format("MMM YYYY"));
          var month_dayStart = moment(moment().subtract(i, 'month')).startOf('month').format("x");
          var month_dayEnd = moment(moment().subtract(i, 'month')).endOf('month').format("x");
          let lineTotalOrderPlaced_query = { where: { status: "OrderPlace", createdAt: { between: [ month_dayStart, month_dayEnd ] }} };
          let lineTotalDeliveredOrder_query = { where: { deliveryStatus: "Delivered", createdAt: { between: [ month_dayStart, month_dayEnd ] }} };
          let lineTotalCancelOrder_query = { where: { deliveryStatus: "Cancelled", createdAt: { between: [ month_dayStart, month_dayEnd ] }} };
          var lineTotalOrderPlaced_list = await ordersRepository.find(lineTotalOrderPlaced_query);
          var lineTotalDeliveredOrder_list = await ordersRepository.find(lineTotalDeliveredOrder_query);
          var lineTotalCancelOrder_list = await ordersRepository.find(lineTotalCancelOrder_query);
          lineTotalOrderPlaced.push(lineTotalOrderPlaced_list.length);
          lineTotalDeliveredOrder.push(lineTotalDeliveredOrder_list.length);
          lineTotalCancelOrder.push(lineTotalCancelOrder_list.length);
        }
      }

      let graphData = {
        lineTotalOrderPlaced: lineTotalOrderPlaced,
        lineTotalDeliveredOrder: lineTotalDeliveredOrder,
        lineTotalCancelOrder: lineTotalCancelOrder,
        lineChartLabels: lineChartLabels
      }
      ctx.res.status(200).send(graphData);

    }




    User.sendForgetPasswordMail = async (ctx, options, cb) => {
      // let requestBody = ctx.req.body;
      
      let requestBody = ctx.req.body;
      // var userRepository = User;
      console.log(requestBody)
      var query = {
        where: {
          email: requestBody.email
        }
      };
      var userData = await User.findOne(query);
      console.log(userData);
      if (userData) {

        var token = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 55) + Math.random().toString(36).substring(2, 55) + Math.random().toString(36).substring(2, 55) + Math.random().toString(36).substring(2, 55);
        console.log(token);
  
        var resetPasswordRepository = User.app.models.ResetPassword;
        let resetTokendataDTO = {
          resetPasswordToken: token,
          userId: userData['id'],
          emailId: userData.email,
        };
        await resetPasswordRepository.create(resetTokendataDTO);
  
        let data = {
          email: userData.email,
          userName: `${userData.firstName} ${userData.lastName}`,
          subject: 'Forgot Password',
          mailType: 'ForgotPassword',
          createToken: token
        }
        await sendingMailService.sendMail_usingSendgrid (data);
        ctx.res.status(200).send( { massage: 'Check your mail to verify your mailId' });
      } else {
        ctx.res.status(404).json({ error: "EmailId Not found" })
      }
    }



    User.verifyResetPasswordToken = async  (ctx, options, cb) => { 
      var tokenDto = ctx.req.body;

      var resetPasswordRepository = User.app.models.ResetPassword;
      var query = {
        where: {
          resetPasswordToken: tokenDto.token
        }
      };

      var resetToken = await resetPasswordRepository.findOne(query);
      if (!resetToken) {
        // throw new HttpErrors.NotFound('Token Not found or Expire');
        ctx.res.status(404).send({
          message: "Token Not found or Expire"
        })
      }
      ctx.res.status(200).send(resetToken);
    }

    User.forgotPasswordChange = async (ctx, options, cb) => {
      var ChangePasswordDto = ctx.req.body;

      if (ChangePasswordDto.newPassword == ChangePasswordDto.confirmNewPassword) { 
        var query = {
          where: {
            resetPasswordToken: ChangePasswordDto.resetPasswordToken,
            emailId: ChangePasswordDto.email,
            userId: ChangePasswordDto.userId,
          },
        };
        var resetPasswordRepository = User.app.models.ResetPassword;
        var resetPass = await resetPasswordRepository.findOne(query);

        if (resetPass) {
          let cred = {
            email: ChangePasswordDto.email,
            password: ChangePasswordDto.newPassword,
          };
  
          // var userRepository = ;
  
          User.findOne({
            where: {
              email: ChangePasswordDto.email
            }
          }, function (err, user) {
            if (user) {
              user.email = ChangePasswordDto.email;
              user.password = ChangePasswordDto.newPassword;
              user.save(function (err, result) {
                if (!err) {
                  ctx.res.status(200).send({ message: "Password Change successfully"})
                } else {
                  ctx.res.status(404).send({ message: "Unable to change password"  })
                }
              })
            } else {
              ctx.res.status(404).send({ message: "Invalid Data" })
            }
          })
        } else {
          ctx.res.status(404).send({ message: "Invalid Data" })
        }
      } else {
        ctx.res.status(404).json({ message: "Password & change password does not match" })
      }

    }

    User.sendVendorApproveMail = async (ctx, options, cb) => {  
      var requestDto = ctx.req.body;

      let userData = await User.findOne({where: {email: requestDto['email']}});

      if (userData) {
        let data = {
          email: userData.email,
          password: requestDto['password'],
          userName: `${userData.firstName} ${userData.lastName}`,
          subject: 'Vendor Approved',
          mailType: 'VendorApproved',
          // createToken: token
        }
        await sendingMailService.sendMail_usingSendgrid (data);
        ctx.res.status(200).send( { massage: 'Mail with email & password is send successfully' });
      } else {
        ctx.res.status(404).json({ error: "EmailId Not found" })
      }


    }



    User.searchEmailAddress = async (ctx, options, cb) => { 
      var emailDto = ctx.req.body;
      let data = await User.find({where: { email: { like: `${emailDto['email']}` } }, limit: 10});
      if (data.length) {
        ctx.res.status(200).json({ unique: false });
      } else {
        ctx.res.status(200).json({ unique: true });
      }      
    }

};

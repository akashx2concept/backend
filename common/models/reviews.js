'use strict';
var moment = require('moment-timezone');

module.exports = function(Reviews) {

    Reviews.validatesInclusionOf('rating', {in: ['1', '2', '3', '4', '5']});

    Reviews.observe('before save', async (ctx, next) => {
        if (ctx.instance && !ctx.instance.id) {
          ctx.instance.createdAt = moment().format('x');
          let requestData = ctx.instance;
          let reviewData = await Reviews.find({ where: { vendorsId: requestData['vendorsId'], userId: requestData['userId'] } });
          // console.log(reviewData);
          if (reviewData.length) {
            return Promise.reject({ error: "Review ALready Given by this user" })
          } else {  
            next()
          }
        }
        if (ctx && ctx.data) {
          ctx.data.updatedAt = moment().format('x');
          next();
        }
        
    });



    Reviews.observe('after save', async (ctx, next) => {
      if (ctx.instance && ctx.instance.id) {  
        let vendorsRepository = Reviews.app.models.Vendors;

        // console.log(ctx.instance, " -- instance");

        let requestData = ctx.instance;

        if (requestData['vendorsId']) {
          // let vendorData = await vendorsRepository.findOne({ where: { id: requestData['vendorsId'] }});
          let reviewData = await Reviews.find({where: {vendorsId: requestData['vendorsId']}});

          var overAllStarRatingdata = {
            five_star: 0,
            four_star: 0,
            three_star: 0,
            two_star: 0,
            one_star: 0,
            overAll_AverageRating: 0,
          }

          for (let q=0; q<=reviewData.length-1; q++) {
            if (reviewData[q]['rating'] == 5) overAllStarRatingdata.five_star = overAllStarRatingdata.five_star + 1;
            if (reviewData[q]['rating'] == 4) overAllStarRatingdata.four_star = overAllStarRatingdata.four_star + 1;
            if (reviewData[q]['rating'] == 3) overAllStarRatingdata.three_star = overAllStarRatingdata.three_star + 1;
            if (reviewData[q]['rating'] == 2) overAllStarRatingdata.two_star = overAllStarRatingdata.two_star + 1;
            if (reviewData[q]['rating'] == 1) overAllStarRatingdata.one_star = overAllStarRatingdata.one_star + 1;
          }
          overAllStarRatingdata.overAllrating = (5 * overAllStarRatingdata.five_star + 4 * overAllStarRatingdata.four_star + 3 * overAllStarRatingdata.three_star + 2 * overAllStarRatingdata.two_star + 1 * overAllStarRatingdata.one_star) / (overAllStarRatingdata.five_star + overAllStarRatingdata.four_star + overAllStarRatingdata.three_star + overAllStarRatingdata.two_star + overAllStarRatingdata.one_star);
          // console.log(overAllStarRatingdata)

          await vendorsRepository.upsert({id: requestData['vendorsId'], rating: overAllStarRatingdata['overAllrating']});


        }

        

      }
    });

};

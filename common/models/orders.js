'use strict';
var moment = require('moment-timezone');

const globalConfig = require('../../server/api/globalConfig');
const stripe = require('stripe')(globalConfig.stripe_Secret_key, globalConfig.stripeOption);


const orderLib = require('order-id')('mysecret');
const uniqueOrderId = orderLib.generate();


module.exports = function(Orders) {

  Orders.validatesInclusionOf('deliveryType', {in: ['Delivery', 'Collection']});
  // Orders.validatesInclusionOf('deliveryCollectDay', {in: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday','Sunday']});
  Orders.validatesInclusionOf('status', {in: ['PaymentIniciated','OrderPlace', 'Confirmed', 'Delivered']});
  Orders.validatesInclusionOf('deliveryStatus', {in: ['PaymentIniciated', 'OrderPlace', 'Confirmed', 'Delivered', 'Cancelled']})

  Orders.observe('before save', function (ctx, next) {
      if (ctx.instance && !ctx.instance.id) {
        ctx.instance.createdAt = moment().format('x');
      }
      if (ctx && ctx.data) {
        ctx.data.updatedAt = moment().format('x');
      }
      next();
  });





  Orders.orderSave = async (requestBody, stripeDto, items_Details) => {
    try {
      console.log("##############################################");
      console.log(requestBody, " -- requestBody");
      console.log(stripeDto, " -- stripeDto");
      console.log(items_Details, " -- items_Details");


      console.log(uniqueOrderId);
      console.log();

      let orderItemRepository = Orders.app.models.OrderItem;
      let orderAddressRepository = Orders.app.models.OrderAddress;

      let orderNumber = orderLib.getTime(uniqueOrderId);

      let orderObj = {
        orderNumber: orderNumber,
        customerNote: requestBody['customerNote'],
        deliveryType: requestBody['deliveryType'] || "Delivery",
        fromTime: null,
        toTime: null,
        // collectionLocation: null,
        // deliveryCollectDay: null,
        totalAmount: items_Details['price_withoutTax'], 
        taxAmount: items_Details['only_tax_amount'],
        taxRate: null,
        discountRate: null,
        deliveryCharges: null,
        grandTotal: items_Details['totalPrice'],
        status: 'PaymentIniciated',
        refundAmount: null,
        refundDeliveryCharge: null,
        refundRemarks: null,
        application_fee_amount: stripeDto['application_fee_amount'],
        orderDate:  moment().format('x'),
        userId: requestBody['userId'],
        vendorsId: requestBody['vendorId'],
        paymentIntentId: stripeDto['id'],
        deliveryStatus: "PaymentIniciated"
      };
      console.log(orderObj, " ---- orderObj");


      let createOrder = await Orders.create(orderObj);

      let productArr = items_Details['finalData'];

      for (let k=0; k<=productArr.length-1; k++) {
        let orderItem_Obj = {
          ordersId: createOrder['id'],
          productName: productArr[k]['productData']['productName'],
          // productImage: ,
          productUnit: productArr[k]['priceData']['productUnit'],
          unitType: productArr[k]['priceData']['unitType'],
          quantity: productArr[k]['quantity'],
          sellingPrice: productArr[k]['priceData']['sellingPrice'],
          totalPrice: productArr[k]['priceData']['unitPrice'],
          isReturn: false,
          userId: requestBody['userId'],
          vendorsId: requestBody['vendorId'],
          productsId: productArr[k]['productData']['id'],
        }
        await orderItemRepository.create(orderItem_Obj);
      }


      let orderAddress_obj = {
        contactPerson: items_Details['shippingAddress']['name'],
        phoneNumber: null,
        postalCode: items_Details['shippingAddress']['address']['postal_code'],
        fullAddress: `${items_Details['shippingAddress']['address']['line1']} ${items_Details['shippingAddress']['address']['line2']} ${items_Details['shippingAddress']['address']['city']} ${items_Details['shippingAddress']['address']['state']} ${items_Details['shippingAddress']['address']['country']}`,
        addressId: items_Details['address']['id'],
        countryId: items_Details['address']['countryId'],
        stateId: items_Details['address']['stateId'],
        cityId: items_Details['address']['cityId'],
        userId: requestBody['userId'],
        ordersId: createOrder['id']
      }

      let s = await orderAddressRepository.create (orderAddress_obj);

      // return s;
    } 
    catch (err) {
      console.log(err);
    }
    

  }



  Orders.paymentStatus = async (ctx, options, cb) => {
    let requestBody = ctx.req.body;
    console.log("sdtfddddddddddddddddddddddddddddddddddddd")
    try {
      let requestBody_userId = requestBody['userId'];
      let requestBody_paymentIntentId = requestBody['paymentIntentId'];

      if (!requestBody_userId) return ctx.res.status(400).send({error: "userId is not present"});
      if (!requestBody_paymentIntentId) return ctx.res.status(400).send({error: "paymentIntentId is not present"})

      let paymentIntent = await stripe.paymentIntents.retrieve(requestBody_paymentIntentId);
      // console.log(paymentIntent);

      let paymentDetailsRepository = Orders.app.models.PaymentDetails;
      let orderData = await Orders.findOne({paymentIntentId: requestBody_paymentIntentId})

      if (orderData) {

        let paymentDetails_obj = await paymentDetailsRepository.findOne({where: {paymentIntentId: requestBody_paymentIntentId}});
        let orderDto = await Orders.findOne({where: { paymentIntentId: requestBody['paymentIntentId'] }})
        let ord = await Orders.upsert({id: orderDto['id'], status: 'OrderPlace', deliveryStatus: "OrderPlace" });
        let paymentData;
        if (paymentDetails_obj && orderDto) {
          let data = {
            id: paymentDetails_obj['id'],
            status: paymentIntent['status'],
            paymentIntentId: paymentIntent['id'],
            paymentObj: JSON.stringify(paymentIntent),
            ordersId: orderDto['id'],
            vendorsId: orderDto['vendorsId'],
            userId: orderDto['userId']
          }
          paymentData = await paymentDetailsRepository.upsert(data);
          // console.log(paymentData, "paymentData");
        } else if (orderDto) {
          let data = {
            status: paymentIntent['status'],
            paymentIntentId: paymentIntent['id'],
            paymentObj: JSON.stringify(paymentIntent),
            ordersId: orderDto['id'],
            vendorsId: orderDto['vendorsId'],
            userId: orderDto['userId']
          }
          
          paymentData = await paymentDetailsRepository.create(data);

          if (paymentIntent['status'] == "succeeded") {
            console.log(paymentIntent['status'])
            let CartItemsRepository = Orders.app.models.CartItems;
            await CartItemsRepository.destroyAll({ where: { userId: orderDto['userId'] } });

            let OrderItemRepository = Orders.app.models.OrderItem;
            let ProductsRepository = Orders.app.models.Products;
    
            let OrderItemData = await OrderItemRepository.find({ where: { ordersId: orderDto['id'] }, include: [ {relation: "products" } ] });
            OrderItemData = JSON.parse(JSON.stringify(OrderItemData));
            
            for (let g=0; g<=OrderItemData.length-1; g++) {
                let productData = JSON.parse(JSON.stringify(OrderItemData[g]['products']));
                // console.log(productData, " -- productData");
                let productUpdate = {
                  id: productData['id'],
                  avaliableStock: parseInt(productData["avaliableStock"]) - parseInt(OrderItemData[g]['quantity']),
                  totalOrderQuentitySale: parseInt(productData['totalOrderQuentitySale']) + parseInt(OrderItemData[g]['quantity']), 
                }
                console.log(productUpdate, " --- productUpdate");
                let de = await ProductsRepository.upsert(productUpdate);
                // console.log(de, " --- de de de");
            }
          }

          // console.log(paymentData, "paymentData");;
        }
        
      


        // console.log(ord, "ord ord");
        // setTimeout(() => {
          ctx.res.status(200).send(ord);
        // },2000)
      } else {
        setTimeout (() => {
          ctx.res.status(400).send({err: "PaymentIntentId Not present "})
        })        
      }


      

      // const paymentIntent = await stripe.paymentIntents.update(
      //   'pi_1IyLH3E1e1bineRZ6kZFg6PU',
      //   {metadata: {order_id: '6735'}}
      // );






    } catch(err)  {
      ctx.res.status(400).send(err);
    }
  
  
  
  }

  
  Orders.orderSalesFessAnalysis = async (ctx, options, cb) => {
    let requestBody = ctx.req.body;
    // console.log("orderSalesFessAnalysis -- ##############################################");
    // console.log(requestBody, " -- requestBody");

    try {
      let query = {
        where: { status: "OrderPlace" },
        fields: ['id', 'totalAmount', 'grandTotal', 'taxAmount', 'application_fee_amount' ]
      }
      if (requestBody['vendorId']) {
        query['where']['vendorsId'] = requestBody['vendorId'] 
      } else {

      }


      let orderData = await Orders.find(query);
      
      let grandTotal = 0;
      let totalApplicationFees = 0;
      let vendorIncome = 0;

      for (let q=0; q<=orderData.length-1; q++) {
        grandTotal = grandTotal + parseInt(orderData[q]['grandTotal']);
        if (orderData[q]['application_fee_amount']) {
          totalApplicationFees = totalApplicationFees + parseInt(orderData[q]['application_fee_amount']);
        }      
      }
      vendorIncome = grandTotal - totalApplicationFees;
      // console.log(orderData);

      ctx.res.status(200).send({
        grandTotal: grandTotal,
        // orderData: orderData,
        vendorIncome: vendorIncome, 
        totalApplicationFees: totalApplicationFees
      });
    }
    catch (err) {
      ctx.res.status(400).send(err);
    }

  }



  Orders.orderStatus = async (ctx, options, cb) => {
    let requestBody = ctx.req.body;
    // console.log("orderSalesFessAnalysis -- ##############################################");
    console.log(requestBody, " -- requestBody");

    try {
      let query = {
        where: { id: requestBody['id'] },
      }
      let orderData = await Orders.findOne(query);
      if (orderData) {
        if (orderData['deliveryStatus'] == "PaymentIniciated") ctx.res.status(400).send({massage: "You can not change the status untill the payment is done"});
        else if (orderData['deliveryStatus'] == "OrderPlace") { 
          if (requestBody['deliveryStatus'] == "Confirmed") {
            let data = { id: requestBody['id'], deliveryStatus: "Confirmed" };
            let upD = await Orders.upsert(data);
            ctx.res.status(200).send({massage: "Order is successfuly Confirmed"});
          } else if (requestBody['deliveryStatus'] == "Cancelled") {
            let paymentIntent = await stripe.refunds.create({
              payment_intent: orderData['paymentIntentId'],
              reason: "requested_by_customer"
            });
            // ctx.res.status(200).send(paymentIntent);
            let data = { id: requestBody['id'], deliveryStatus: "Cancelled" };
            let upD = await Orders.upsert(data);
            ctx.res.status(200).send({massage: `Order is successfuly Cancelled Refund of € ${paymentIntent['amount'] / 100} is initiated`});
          } else {
            ctx.res.status(400).send({massage: `you can not mark this order ${requestBody['deliveryStatus']} untill it is Confirmed`})
          }
        }
        else if (orderData['deliveryStatus'] == "Confirmed") {
          if (requestBody['deliveryStatus'] == "Delivered") {
            let data = { id: requestBody['id'], deliveryStatus: "Delivered" };
            let upD = await Orders.upsert(data);
            ctx.res.status(200).send({massage: "Order is successfuly Delivered"});
          } else {
            ctx.res.status(400).send({massage: `Order is already Confirmed, You can only mark this order as ${requestBody['deliveryStatus']}`});
          }
        }
        else if (orderData['deliveryStatus'] == "Delivered") {
          ctx.res.status(400).send({massage: "Order is already Delivered, You can not chenge the status further"});
        }
        else if (orderData['deliveryStatus'] == "Cancelled") {
          ctx.res.status(400).send({massage: "Order is already Cancelled, You can not chenge the status further"});
        }
      } else {
        ctx.res.status(400).send({massage: "No order with this id found"});
      }
    }
    catch(err) {
      ctx.res.status(400).send(err);
    }
  }

  Orders.viewOrderBill = async (ctx, options, cb) => { 
    let requestBody = ctx.req.body;
    console.log(requestBody, " -- requestBody");

    try {
      let orderData = await Orders.findOne({where: {id: requestBody['orderId']}});
      if (orderData) {
        if (orderData['paymentIntentId']) {
          let paymentIntent = await stripe.charges.list({payment_intent: orderData['paymentIntentId']} );
          ctx.res.status(200).send(paymentIntent);
        } else {
          ctx.res.status(400).send({massage: "paymentIntentId not present in order"});
        }
      } else {
        ctx.res.status(400).send({massage: "No order with this id found"});
      }
    }
    catch (err) {
      ctx.res.status(400).send(err);
    }
    





  }


};

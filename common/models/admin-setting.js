'use strict';
var moment = require('moment-timezone');

module.exports = function(Adminsetting) {

    Adminsetting.validatesInclusionOf('metaKey', {in: ['transactionCharge', 'commissionRate', 'enableCommission', 'enable2fa', 'termCondition', 'privacyPolicy']});

    Adminsetting.observe('before save', function (ctx, next) {
        if (ctx.instance && !ctx.instance.id) {
          ctx.instance.createdAt = moment().format('x');
        }
        if (ctx && ctx.data) {
          ctx.data.updatedAt = moment().format('x');
        }
        next();
    });


    Adminsetting.termAndCondition = async (ctx, options, cb) => { 
      // let termAndCondition = await Adminsetting.findOne ( {where: { metaKey: "termCondition" }} );
      ctx.res.status(200).send(await Adminsetting.findOne ( {where: { metaKey: "termCondition" }}));
    }

    Adminsetting.privacyPolicy =  async (ctx, options, cb) => {  
      // let privacyPolicy = await Adminsetting.findOne ( {where: { metaKey: "privacyPolicy" }} );
      ctx.res.status(200).send(await Adminsetting.findOne ( {where: { metaKey: "privacyPolicy" }} ));
    }

};

'use strict';
var moment = require('moment-timezone');


module.exports = function(Productprice) {

    Productprice.observe('before save', function (ctx, next) {
        if (ctx.instance && !ctx.instance.id) {
          ctx.instance.createdAt = moment().format('x');
        }
        if (ctx && ctx.data) {
          ctx.data.updatedAt = moment().format('x');
        }
        next();
    });



    Productprice.observe('after save', async (ctx, next) => { 
      if (ctx.instance && ctx.instance.id) {
        ctx.instance.updatedAt = moment().format('x');

        // Productprice.app.models.StripePayment.createVendorProductPrice(ctx.instance['id']);
      }
    })

};

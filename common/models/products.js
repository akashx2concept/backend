'use strict';
var moment = require('moment-timezone');

module.exports = function(Products) {

    Products.observe('before save', async (ctx, next) => {
        if (ctx.instance && !ctx.instance.id) {
          ctx.instance.createdAt = moment().format('x');
        }
        if (ctx && ctx.data) {
          ctx.data.updatedAt = moment().format('x');
        }
        next();
    });

    
    Products.observe('after save', async (ctx, next) => {
      if (ctx.instance && ctx.instance.id) {
        // await Products.app.models.StripePayment.createVendorProduct(ctx.instance['id']);
        
        ctx.instance.updatedAt = moment().format('x');
        // console.log(ctx.instance, "ctx.instance ###");
        let createdData = ctx.instance;
        // console.log(createdData)
        let productTagsRepository  = Products.app.models.ProductTags;
        let productPriceRepository = Products.app.models.ProductPrice;
        
        let tags = createdData.tags;
        let price = ctx.instance.price;
        if (tags && tags.length) {
          for (let f=0; f<=tags.length-1; f++) {
            if (!tags[f]['id']) {
              let dataDto = { productsId: createdData.id, tag: tags[f]['tag'] }
              await productTagsRepository.create(dataDto);
            } else {
              let dataDto = { id: tags[f]['id'], productsId: createdData.id, tag: tags[f]['tag'] }
              await productTagsRepository.upsert(dataDto);
            }
          }
        }
        if (price && price.length) {
          for (let f=0; f<=price.length-1; f++) {
            if (!price[f]['id']) { 
              let dataDto = { productsId: createdData.id, vendorsId: createdData['vendorsId'], productUnit: price[f]['productUnit'], unitPrice: price[f]['unitPrice'], unitType: price[f]['unitType'], sellingPrice: price[f]['sellingPrice'] }
              await productPriceRepository.create(dataDto);
            } else {
              let dataDto = { id: price[f]['id'], vendorsId: createdData['vendorsId'], productsId: createdData.id, productUnit: price[f]['productUnit'], unitPrice: price[f]['unitPrice'], unitType: price[f]['unitType'], sellingPrice: price[f]['sellingPrice'] }
              await productPriceRepository.upsert(dataDto);
            }
          }
        }

        // for creating product on the stripe
        
      }
      next();
    })


};

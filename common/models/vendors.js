'use strict';
var moment = require('moment-timezone');

module.exports = function(Vendors) {


    // Vendors.validatesInclusionOf('currentStatus', {in: ['Approve']});

    const vendorSettingArray = [
      {
        slug: "available",
        value: false
      },
      {
        slug: "show_full_address",
        value: true
      },
      {
        slug: "hide_store_time",
        value: false
      },
      {
        slug: "turn_off_orders",
        value: false
      },
      {
        slug: "enable_2fa",
        value: false
      },
      {
        slug: "min_order_amount",
        value: 1
      }
    ]


    Vendors.observe('before save', function (ctx, next) {
        if (ctx.instance && !ctx.instance.id) {
          ctx.instance.createdAt = moment().format('x');
        }
        if (ctx && ctx.data) {
          ctx.data.updatedAt = moment().format('x');
        }
        next();
    });


    Vendors.observe('after save', async (ctx, next) => {
      if (ctx.instance && ctx.instance.id) { 
        
        let VendorSettingRepository = Vendors.app.models.VendorSetting;
        
        for (let k=0; k<=vendorSettingArray.length-1; k++) {
          
          let queryFind = {
            where: {
              vendorsId: ctx.instance.id,
              slug: vendorSettingArray[k]['slug']
            }            
          }
          let findData = await VendorSettingRepository.findOne(queryFind);
          
          if (!findData) {
            let createData = {
              vendorsId: ctx.instance.id,
              slug: vendorSettingArray[k]['slug'],
              value: vendorSettingArray[k]['value']
            } 
            var d = await VendorSettingRepository.create(createData);
            console.log(d);
          } else {

          }
        }
        

      }
    });

};

'use strict';
var moment = require('moment-timezone');
const globalConfig = require('../../server/api/globalConfig');
const stripe = require('stripe')(globalConfig.stripe_Secret_key, globalConfig.stripeOption);

const stripePayment_successReturnUrl = globalConfig.stripePayment_successReturnUrl;
const stripePayment_refreshUrl = globalConfig.stripePayment_refreshUrl;

module.exports = function(StripePayment) {

  
  StripePayment.observe('before save', function (ctx, next) {
    if (ctx.instance && !ctx.instance.id) {
      ctx.instance.createdAt = moment().format('x');
    }
    if (ctx && ctx.data) {
      ctx.data.updatedAt = moment().format('x');
    }
    next();
  });


    StripePayment.onboardVendorRefresh = async (ctx, options, cb) => {
        console.log("$$$$$$$$$$$$$$$$$$$$$$$$$$$");
        console.log(ctx.req.body)
          try {
            const accountID = ctx.req.body.accountId;
            console.log(accountID);
            const origin = `${ctx.req.secure ? "https://" : "https://"}${globalConfig.stripePayment_successReturnUrl}`;
            const accountLinkURL = await generateAccountLink(accountID)
            // res.redirect();
            
            ctx.res.status(200).send({url: accountLinkURL})
          } catch (err) {
            ctx.res.status(500).send({
              error: err.message
            });
        }
    }

    StripePayment.getConnectedAccountData = async (ctx, options, cb) => {
      try {
        let vendorId = ctx.req.body.vendorId;
        let vendorRepository = StripePayment.app.models.Vendors;
        let vendorData = await vendorRepository.findOne({where: {id: vendorId}});
        if (vendorData['stripeConnectedAccountId']) {
          let stripeConnectedAccountData = await stripe.accounts.retrieve(vendorData['stripeConnectedAccountId']);
          ctx.res.status(200).send(stripeConnectedAccountData)
        } else {
          ctx.res.status(400).send({ error: "Vendor doesn't contain connected accounId" });
        }
      } catch (err) {
          ctx.res.status(500).send({
            error: err.message
          });
      }
    }

    StripePayment.getConnectedAccount_loginLink = async (ctx, options, cb) => {
      try {
        let vendorId = ctx.req.body.vendorId;
        let vendorRepository = StripePayment.app.models.Vendors;
        let vendorData = await vendorRepository.findOne({where: {id: vendorId}});
        if (vendorData['stripeConnectedAccountId']) {
          let stripeConnectedAccountData = await stripe.accounts.createLoginLink(vendorData['stripeConnectedAccountId']);
          ctx.res.status(200).send(stripeConnectedAccountData)
        } else {
          ctx.res.status(400).send({ error: "Vendor doesn't contain connected accounId" });
        }
      } catch (err) {
          ctx.res.status(500).send({
            error: err.message
          });
      }
    }

     StripePayment.onboardVendor = async (ctx, options, cb) => {
        try {
            let requestBody = ctx.req.body;
            console.log(requestBody);
            if (requestBody['vendorId']) {
                let stripePaymentRepository = StripePayment.app.models.StripePayment;
                let vendorRepository = StripePayment.app.models.Vendors;
                let data = await stripePaymentRepository.findOne({where: { vendorsId: requestBody['vendorId'] }})
                console.log(data);
                if (data) {
                    let da = { id: data['id'], vendorsId: requestBody['vendorId'], setupUrl: data['setupUrl'], accountId: data['accountId'] };
                    let dataDto = await stripePaymentRepository.upsert(da);
                    console.log(dataDto);
                    return dataDto;
                } else {
                    const account = await stripe.accounts.create({type: "express"});
                    console.log(account);
                    const accountLinkURL = await generateAccountLink(account.id);
                    console.log(accountLinkURL);
                    let da = { vendorsId: requestBody['vendorId'], setupUrl: accountLinkURL, accountId: account.id };
                    let vendorSaveData = { id: requestBody['vendorId'], stripeConnectedAccountId: account.id }
                    await vendorRepository.upsert(vendorSaveData);
                    let dataDto = await stripePaymentRepository.create(da);
                    console.log(dataDto);
                    return dataDto;
                }
            } 
            else { return {massgae: "Something went wrong"} }          
        } 
        catch (err) { return  {error: err.message}; }
    };



    async function generateAccountLink(accountID) {
        return stripe.accountLinks.create({
          type: "account_onboarding",
          account: accountID,
          refresh_url: `${stripePayment_refreshUrl}/api/StripePayments/onboardVendorRefresh`,
          return_url: `${stripePayment_successReturnUrl}/socialMediaLogin`,
        }).then((link) => link.url);
    }   



    // for creating customer on stripe
    StripePayment.createCustomerStripe = async (userId) => {
      console.log(userId, "userData");
      let userRepository = StripePayment.app.models.user;
      let findUser = await userRepository.findOne({where: { id: userId }});

      if (findUser['role'] == 'User') {
        let stripeCustomerDto = {
          name: null,
          email: findUser['email'],
        }
        if (!findUser['stripeCustomerId']) {
          let createStripeCustomer = await stripe.customers.create(stripeCustomerDto);
          console.log(createStripeCustomer);
          return await userRepository.upsert({ id: userId, stripeCustomerId: createStripeCustomer['id'] });
        }
      } else {
        return 
      }      
    }


    // for creating customer card in stripe
    StripePayment.createCustomerCard_stripe = async (ctx, options, cb) => {
      let requestBody = ctx.req.body;
      try {
        if (requestBody['CardHolderName'] && requestBody['CardNumber'] && requestBody['expMonth'] && requestBody['expYear'] && requestBody['userId'] ) {

          let userRepository = StripePayment.app.models.user;
          let findUser = await userRepository.findOne({where: { id: requestBody['userId'] }});

          if (findUser['stripeCustomerId']) {
            let cardDto = {
              name: requestBody['CardHolderName'],
              number: requestBody['CardNumber'],
              exp_month: requestBody['expMonth'],
              exp_year: requestBody['expYear']
            }
            console.log(cardDto);
            // let createdCard = await stripe.customers.createSource(findUser['stripeCustomerId'], {source: cardDto});

            let getCardToken = await stripe.tokens.create({ card: cardDto });
            let createdCard = await stripe.customers.createSource(findUser['stripeCustomerId'], {source: getCardToken['id']});
            ctx.res.status(200).send(createdCard)
          } 
          else { ctx.res.status(422).send(new Error("Stripe Customer Id is not present")) }
        } 
        else { ctx.res.status(422).send(new Error("Required Filed Missing 'CardHolderName' &  'CardNumber', 'expMonth', 'expYear', 'userId' ")) }
      }
      catch(err) { ctx.res.status(422).send({ error: err.message }); }
    }


    // for getting customer card from stripe
    StripePayment.getStripeCustomerCard = async (ctx, options, cb) => {
      let requestBody = ctx.req.body;
      try {
        if (requestBody['userId'] ) {
          let userRepository = StripePayment.app.models.user;
          let findUser = await userRepository.findOne({where: { id: requestBody['userId'] }});

          if (findUser['stripeCustomerId']) {
            let stripeCustomerId = findUser['stripeCustomerId']
            const cards = await stripe.customers.listSources(stripeCustomerId, {object: 'card'});
            ctx.res.status(200).send(cards);
          }
          else { ctx.res.status(422).send(new Error("Stripe Customer Id is not present")); }
        }
        else { ctx.res.status(422).send(new Error("Required Filed Missing 'userId' ")); }
      }
      catch(err) { ctx.res.status(422).send({ error: err.message }); }
    }

    // For creating & update vendor Product on stripe
    StripePayment.createVendorProduct = async (productId) => {
      console.log(productId);
      try {
        if (productId) {
          let productRepository = StripePayment.app.models.Products;
          let vendorRepository = StripePayment.app.models.Vendors;
          let productData = await productRepository.findOne({where: {id: productId}});
          // console.log(productData, "productData");
  
          if (!productData['stripeProductId']) {
            if (productData['vendorsId']) {
              let vendorData = await vendorRepository.findOne({where: {id: productData['vendorsId'] }});
              // console.log(vendorData, "vendorData");
              if (vendorData['stripeConnectedAccountId']) {
                let productStripeData = await stripe.products.create({ name: productData['productName'] }, 
                // { stripeAccount: vendorData['stripeConnectedAccountId'],}
                );
                // console.log(productStripeData)
                let productUpdate = {id: productData['id'], stripeProductId: productStripeData['id'] }
                let p = await productRepository.update(productUpdate);
                // console.log(p, " ---- p")
                await method_for_syncing_product_with_stripe(productStripeData['id'], productData, vendorData['stripeConnectedAccountId']);
                return true;
              } 
              else { console.log("Vendor Stripe Connected Account not present"); }
            } 
            else { console.log("Vendor ID not present"); }
          } else {
            let vendorData = await vendorRepository.findOne({where: {id: productData['vendorsId'] }});
            // console.log(vendorData, "vendorData");
            if (vendorData['stripeConnectedAccountId']) {
              await method_for_syncing_product_with_stripe(productData['stripeProductId'], productData,  vendorData['stripeConnectedAccountId']);
              return true;
            }           
          }        
        } 
        else { console.log("Product ID not present"); }
      }
      catch (err) {
        console.log("##################################");
        console.log(err, "createVendorProduct");
        console.log("##################################");
      }
    }


    // method for syncing product with strip product
    async function method_for_syncing_product_with_stripe (productStripeId, productData, stripeConnectedAccountId) {
      // console.log(productStripeId, "  productStripeId");
      // console.log(productData, "  productData");
      // console.log(stripeConnectedAccountId, "stripeConnectedAccountId");

      let stripeProductDto = {
        name: productData['productName'],
        description: productData['description'],
        active: productData['isActive'],
      }
      let product = await stripe.products.update(productStripeId, stripeProductDto,  
        // { stripeAccount: stripeConnectedAccountId}
      );
      console.log(product);
      return product;
    }



    StripePayment.createVendorProductPrice = async (priceId) => {
      console.log(priceId);
      try {
        if (priceId) {
          let vendorRepository = StripePayment.app.models.Vendors;
          let productRepository = StripePayment.app.models.Products;
          let productPriceRepository = StripePayment.app.models.ProductPrice;
          
          let priceData = await productPriceRepository.findOne({where: {id: priceId}});
          let productData = await productRepository.findOne({where: {id: priceData['productsId']}});
          let vendorData = await vendorRepository.findOne({where: {id: productData['vendorsId']}});


          
          if (!priceData['stripeProductPriceId']) {
            if (productData['stripeProductId']) { 
              if (vendorData['stripeConnectedAccountId']) {
                let createProductPriceObj = { product: productData['stripeProductId'], unit_amount: parseInt(priceData['sellingPrice'])*100, currency: 'eur' }
                let productPriceStripeData = await stripe.prices.create(createProductPriceObj, 
                  // { stripeAccount: vendorData['stripeConnectedAccountId'], }
                );
                console.log(productPriceStripeData);
                let productPriceUpdate = {id: priceData['id'], stripeProductPriceId: productPriceStripeData['id'] }
                let p = await productPriceRepository.update(productPriceUpdate);
                console.log(p);
                // await method_for_syncing_product_with_stripe(productPriceStripeData['id'], priceData, vendorData['stripeConnectedAccountId']);
                return true;
              } else { 
                console.log("Vendor Stripe Connected Account not present");
              }
            } else {
              // await StripePayment.createVendorProduct(productData['id']);
              // await StripePayment.createVendorProductPrice(priceId);
            }
          } else {
            // await method_for_syncing_product_with_stripe(priceData['stripeProductPriceId'], priceData, vendorData['stripeConnectedAccountId']);
            return true;
          }
        }
      }
      catch(err) { 
        console.log("##################################");
        console.log(err, "createVendorProductPrice");
        console.log("##################################");
      }
    }

    async function method_for_syncing_product_with_stripe (stripeProductPriceId, priceData, stripeConnectedAccountId) {
      console.log(stripeProductPriceId, "  productStripeId");
      console.log(priceData, "  priceData");
      console.log(stripeConnectedAccountId, "stripeConnectedAccountId");

      let stripeProductPriceDto = { 
        // product: productData['stripeProductId'], 
        unit_amount: priceData['sellingPrice'], 
        currency: 'eur'
      }

      let productPrice = await stripe.prices.update(stripeProductPriceId, stripeProductPriceDto, 
        // { stripeAccount: stripeConnectedAccountId } 
      )
      console.log(productPrice);
      return productPrice;
    }



    StripePayment.customerPlaceOrder = async (ctx, options, cb) => {
      let requestBody = ctx.req.body;
      console.log(requestBody, "dddddddddddddddddddddddddddddddd")
      try {
        let requestBody_userId = requestBody['userId'];
        let requestBody_userCardId = requestBody['userCardId'];
        let requestBody_vendorId = requestBody['vendorId'];
        let requestBody_product = requestBody['product'];
        let requestBody_addressId = requestBody['addressId'];

        if (!requestBody_userId) return ctx.res.status(400).send({error: "userId is not present"});
        // if (!requestBody_userCardId) return ctx.res.status(400).send({error: "userCardId is not present"});
        if (!requestBody_vendorId) return ctx.res.status(400).send({error: "vemdorId is not present"});
        if (!requestBody_product) return ctx.res.status(400).send({error: "product is not present"});
        if (!requestBody_product.length) return ctx.res.status(400).send({error: "Atlest 1 product is required to place order"});
        if (!requestBody_addressId) return ctx.res.status(400).send({error: "addressId is required "});
        

        let userRepository = StripePayment.app.models.user;
        let vendorRepository = StripePayment.app.models.Vendors;
        let adminSettingRepository = StripePayment.app.models.AdminSetting;
        let addressRepository = StripePayment.app.models.Address;

        let userData = await userRepository.findOne({where:{id: requestBody_userId}});
        let vendorData = await vendorRepository.findOne({where: {id: requestBody_vendorId}});
        let addressData = await addressRepository.findOne({ where: { id: requestBody_addressId }, include: [ { relation: "country" }, { relation: "state" }, { relation: "city" } ] });
        addressData = JSON.parse(JSON.stringify(addressData));

        let stripeCustomerId = userData['stripeCustomerId'];
        if (!stripeCustomerId) return ctx.res.status(400).send({error: "stripeCustomerId is not present in user"});

        let stripeConnectedAccountId = vendorData['stripeConnectedAccountId'];
        if (!stripeConnectedAccountId) return ctx.res.status(400).send({error: "Vendor doennot have stripeConnectedAccountId"});

        let line_items_Details = []
        line_items_Details = await method_for_making_product_price_array(requestBody_vendorId, requestBody_product);

        // console.log(line_items_Details);

        line_items_Details["address"] = addressData;


        let shippingAddress = {
          name: null,
          address: {
            line1: null,
            line2: null,
            city: null,
            state: null,
            country: null,
            postal_code: null
          }
        }
        shippingAddress.name = addressData.name || `${userData.firstName || ''} ${userData.middleName || ''} ${userData.lastName || ''} `;
        shippingAddress.address.line1 = addressData.street1 || '';
        shippingAddress.address.line2 = addressData.street2 || '';

        if (addressData.countryId) shippingAddress.address.country = addressData.country.name;
        if (addressData.stateId) shippingAddress.address.state = addressData.state.name;
        if (addressData.cityId) shippingAddress.address.city = addressData.city.name;
        shippingAddress.address.postal_code = addressData.postalCode;


        line_items_Details["shippingAddress"] = shippingAddress;
        
        // ctx.res.status(200).send(line_items_Details);

        let dataDto = {
          amount: parseInt(line_items_Details['totalPrice']),
          currency: 'eur',
          customer: userData['stripeCustomerId'],
          application_fee_amount: parseInt(line_items_Details['totalComission']),
          transfer_data: {
              destination: stripeConnectedAccountId,
          },
          // payment_method: requestBody_userCardId,
          payment_method_types: ['card'],
          on_behalf_of: stripeConnectedAccountId,
          use_stripe_sdk: true,
          shipping: shippingAddress,
        }
        // console.log(dataDto, "sdgfgdfgdgd")
        const data = await stripe.paymentIntents.create(dataDto);
        // console.log(data, "sdgfhf")
        await StripePayment.app.models.Orders.orderSave(requestBody, data, line_items_Details);  


        // const paymentIntent = await stripe.paymentIntents.confirm(
        //   data['id'],
        //   {payment_method: 'pm_card_visa'}
        // );


        ctx.res.status(200).send(data);
        // setTimeout(() => {
          
        // }, 1000)
        // await 
      } catch (err) {
        ctx.res.status(400).send(err);
      }

    }



    async function method_for_making_product_price_array (vendorsId, requestBody_product) {
      let vendorRepository = StripePayment.app.models.Vendors;
      let productRepository = StripePayment.app.models.Products;
      let productPriceRepository = StripePayment.app.models.ProductPrice;

      let finalData = [];
      for (let a=0; a<=requestBody_product.length-1; a++) {
        let productData = await productRepository.findOne({ where: { id: requestBody_product[a]['productId'], vendorsId: vendorsId } });
        let productPrice = await productPriceRepository.findOne({where: { id: requestBody_product[a]['productPrices'], productsId: requestBody_product[a]['productId'] }})
        let c = {
          productId: requestBody_product[a]['productId'],
          productData: productData,
          priceData: productPrice,
          productPrice: productPrice['sellingPrice'],
          quantity: parseInt(requestBody_product[a]['quantity']),
          price: productPrice['stripeProductPriceId'],
          priceWithoutTax: productPrice['unitPrice']
        }
        finalData.push(c);
      }

      let totalPrice = 0; //selling price with taxes
      let price_withoutTax = 0; // selling price without taxes
      let only_tax_amount = 0; // tax amount
      for (let q=0; q<=finalData.length-1; q++) {
        totalPrice = totalPrice + parseInt(finalData[q]['productPrice']) * finalData[q]['quantity'];
        price_withoutTax = price_withoutTax + parseInt(finalData[q]['priceWithoutTax']) * finalData[q]['quantity'];
      }

      let totalComission = await method_for_finding_comission(vendorsId, totalPrice);
      only_tax_amount = totalPrice - price_withoutTax;

      return {
        only_tax_amount: parseInt(only_tax_amount)*100,
        price_withoutTax: parseInt(price_withoutTax)*100,
        totalComission: parseInt(totalComission)*100,
        finalData: finalData,
        totalPrice: parseInt(totalPrice)*100
      };
    }

    async function method_for_finding_comission (vendorId, totalPrice) {
      let vendorRepository = StripePayment.app.models.Vendors;
      let vendorCommissionRateRepository = StripePayment.app.models.VendorCommissionRate;
      let adminSettingRepository = StripePayment.app.models.AdminSetting;

      // let vendorData = await vendorRepository.findOne({where: {id: vendorId}});

      let vendorCommissionRateData = await vendorCommissionRateRepository.findOne({where: { vendorsId: vendorId, enableCommission: true }});
      
      let percentageCostAdd = 0;
      if (vendorCommissionRateData) {
        let costAdd = (parseInt(totalPrice) * parseInt(vendorCommissionRateData['commissionRate'])) / 100;
        percentageCostAdd = costAdd + parseInt(vendorCommissionRateData['transactionCharge']);
      } else {
        let transactionChargeData = await adminSettingRepository.findOne({where: { metaKey: 'transactionCharge' }});
        let commissionRateData = await adminSettingRepository.findOne({where: { metaKey: 'commissionRate' }});
        // console.log(transactionChargeData);
        // console.log(commissionRateData);

        let costAdd = (parseInt(totalPrice) * parseInt(commissionRateData['metaValue'])) / 100;
        percentageCostAdd = costAdd + parseInt(transactionChargeData['metaValue']);
      }

      


      return percentageCostAdd;
    }

};
'use strict';
var moment = require('moment-timezone');

module.exports = function(Becomevendor) {

    Becomevendor.validatesInclusionOf('currentStatus', {in: ['Requested', 'Approved', 'Rejected']});

    Becomevendor.observe('before save', function (ctx, next) {
        if (ctx.instance && !ctx.instance.id) {
          ctx.instance.createdAt = moment().format('x');
        }
        if (ctx && ctx.data) {
          ctx.data.updatedAt = moment().format('x');
        }
        next();
    });

};

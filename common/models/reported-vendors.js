'use strict';
var moment = require('moment-timezone');

module.exports = function(Reportedvendors) {


    Reportedvendors.observe('before save', async (ctx, next) => {
        if (ctx.instance && !ctx.instance.id) {
          ctx.instance.createdAt = moment().format('x');
        }
        if (ctx && ctx.data) {
          ctx.data.updatedAt = moment().format('x');
        }
        next();
    });


};

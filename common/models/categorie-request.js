'use strict';
var moment = require('moment-timezone');

module.exports = function(Categorierequest) {

    Categorierequest.validatesInclusionOf('status', {in: ['Requested', 'Approve', 'Rejected']});

    Categorierequest.observe('before save', function (ctx, next) {
        if (ctx.instance && !ctx.instance.id) {
          ctx.instance.createdAt = moment().format('x');
        }
        if (ctx && ctx.data) {
          ctx.data.updatedAt = moment().format('x');
        }
        next();
    });


};

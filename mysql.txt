CREATE TABLE `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `firstName` varchar(512) NULL,
  `middleName` varchar(512)  NULL,
  `lastName` varchar(512)  NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(512) DEFAULT NULL,
  `country` varchar(512) DEFAULT NULL,
  `isNewUser` tinyint(1) DEFAULT NULL,
  `isActive` tinyint(1) DEFAULT NULL,
  `isDeleted` tinyint(1) DEFAULT NULL,
  `isEmailVerified` tinyint(1) DEFAULT NULL,
  `isEmailSubscribe` tinyint(1) DEFAULT NULL,
  `createdAt` varchar(512) DEFAULT NULL,
  `updatedAt` varchar(512) DEFAULT NULL,
  `companyId` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ;


ALTER TABLE user Add column verificationToken varchar(255);
ALTER TABLE user Add column username varchar(255);
ALTER TABLE user Add column emailVerified int(1);
ALTER TABLE user Add column realm varchar(255);


CREATE TABLE `acl` (
  `id` int NOT NULL AUTO_INCREMENT,
  `model` varchar(512) NOT NULL,
  `property` varchar(512) DEFAULT NULL,
  `accessType` varchar(512) DEFAULT NULL,
  `permission` varchar(512) DEFAULT NULL,
  `principalType` varchar(512) DEFAULT NULL,
  `principalId` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ;



CREATE TABLE `AccessToken` (
  `id` varchar(512) NOT NULL,
  `ttl` int DEFAULT NULL,
  `created` varchar(512) DEFAULT NULL,
  `userId` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ;
ALTER TABLE AccessToken ADD COLUMN `scopes` TEXT NULL;
ALTER TABLE AccessToken MODIFY created datetime;



CREATE TABLE `country` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(512) DEFAULT NULL,
  `phone_code` varchar(512) DEFAULT NULL,
  `currency` varchar(512) DEFAULT NULL,
  `currency_symbol` varchar(255) DEFAULT NULL,
  `latitude` varchar(512) DEFAULT NULL,
  `longitude` varchar(512) DEFAULT NULL,
  `emoji` varchar(512) DEFAULT NULL,
  `emojiU` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `state` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(512) DEFAULT NULL,
  `country_code` varchar(512) DEFAULT NULL,
  `state_code` varchar(512) DEFAULT NULL,
  `latitude` varchar(255) DEFAULT NULL,
  `longitude` varchar(512) DEFAULT NULL,
  `countryId` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ;


CREATE TABLE `city` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(512) DEFAULT NULL,
  `country_code` varchar(512) DEFAULT NULL,
  `state_code` varchar(512) DEFAULT NULL,
  `latitude` varchar(255) DEFAULT NULL,
  `longitude` varchar(512) DEFAULT NULL,
  `countryId` int DEFAULT NULL,
  `stateId` int DEFAULT NULL,
  PRIMARY KEY (`id`)
);


ALTER TABLE country CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_bin;
ALTER TABLE state CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_bin;
ALTER TABLE city CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_bin;



ALTER TABLE user Add column role varchar(255);




CREATE TABLE `Address` (
  `id` int NOT NULL AUTO_INCREMENT,
  `street1` varchar(512) NULL,
  `street2` varchar(512)  NULL,
  `userId` int DEFAULT NULL,
  `countryId` int DEFAULT NULL,
  `stateId` int DEFAULT NULL,
  `cityId` int DEFAULT NULL,
  `isActive` tinyint(1) DEFAULT NULL,
  `isDeleted` tinyint(1) DEFAULT NULL,
  `createdAt` varchar(512) DEFAULT NULL,
  `updatedAt` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

ALTER TABLE Address Add column postalCode varchar(255);


CREATE TABLE `Categories` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(512) NULL,
  `type` varchar(512)  NULL,
  `isActive` tinyint(1) DEFAULT true,
  `isDeleted` tinyint(1) DEFAULT false,
  `createdAt` varchar(512) DEFAULT NULL,
  `updatedAt` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
);



CREATE TABLE `Vendors` (
  `id` int NOT NULL AUTO_INCREMENT,
  `businessName` varchar(512) NULL,
  `vendorName` varchar(512)  NULL,
  `venderMobileNo` varchar(512)  NULL,
  `designation` varchar(512)  NULL,
  `RegistrationNo` varchar(512)  NULL,
  `businessAddress` TEXT  NULL,
  `businessGoogleAddress` TEXT  NULL,
  `postalCode` varchar(512)  NULL,
  `aboutUs` TEXT  NULL,
  `storeName` varchar(512)  NULL,
  `storeAddressType` varchar(512)  NULL,
  `storeAddress` TEXT  NULL,
  `localDelivery`  tinyint(1) DEFAULT false,
  `clickCollect`  tinyint(1) DEFAULT false,
  `deliveryPolicy` TEXT  NULL,
  `collectionPolicy` TEXT  NULL,
  `countryId` int DEFAULT NULL,
  `stateId` int DEFAULT NULL,
  `cityId` int DEFAULT NULL,
  `userId` int DEFAULT NULL,
  `isActive` tinyint(1) DEFAULT true,
  `isDeleted` tinyint(1) DEFAULT false,
  `createdAt` varchar(512) DEFAULT NULL,
  `updatedAt` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

ALTER TABLE user Add column vendorDesignation varchar(255);
ALTER TABLE Vendors Add column categoriesId varchar(255);

ALTER TABLE Vendors Add column latitude varchar(255);
ALTER TABLE Vendors Add column longitude varchar(255);


CREATE TABLE `VendorTimings` (
  `id` int NOT NULL AUTO_INCREMENT,
  `weekDay` varchar(512) NULL,
  `startTime` varchar(512) NULL,
  `endTime` varchar(512) NULL,
  `vendorsId` varchar(512) NULL,
  `createdAt` varchar(512) DEFAULT NULL,
  `updatedAt` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
);



# 21 May 2021
CREATE TABLE `Products` (
  `id` int NOT NULL AUTO_INCREMENT,
  `productName` varchar(512) NULL,
  `allergens` TEXT  NULL,
  `ageRange` varchar(512)  NULL,
  `avaliableStock` varchar(512)  NULL,
  `vatAvailable` tinyint(1) DEFAULT false,
  `vatRate` varchar(512)  NULL,
  `expiryDate` varchar(512)  NULL,
  `status`  tinyint(1) DEFAULT true,
  `urlSlug` varchar(512)  NULL,
  `vendorsId` varchar(512) NULL,
  `userId`varchar(512) NULL,
  `isActive` tinyint(1) DEFAULT true,
  `isDeleted` tinyint(1) DEFAULT false,
  `createdAt` varchar(512) DEFAULT NULL,
  `updatedAt` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `urlSlug` (`urlSlug`)
);


CREATE TABLE `FavoriteVendor` (
  `id` int NOT NULL AUTO_INCREMENT,
  `vendorsId` varchar(512) NULL,
  `userId`varchar(512) NULL,
  `isActive` tinyint(1) DEFAULT true,
  `isDeleted` tinyint(1) DEFAULT false,
  `createdAt` varchar(512) DEFAULT NULL,
  `updatedAt` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `StorageFile` (
  `id` int NOT NULL AUTO_INCREMENT,
  `url` varchar(512) NULL,
  `type` varchar(512) NULL,
  `name` varchar(512) NULL,
  `createdAt` varchar(512) DEFAULT NULL,
  `updatedAt` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
);


# 22 May 2021
ALTER TABLE user Add column profilePicId int DEFAULT NULL;

CREATE TABLE `ProductImage` (
  `id` int NOT NULL AUTO_INCREMENT,
  `productsId` int DEFAULT NULL,
  `productImageId` int DEFAULT NULL,
  `createdAt` varchar(512) DEFAULT NULL,
  `updatedAt` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

ALTER TABLE Products Add column description TEXT DEFAULT NULL;
ALTER TABLE Products Add column categoriesId int DEFAULT NULL;



CREATE TABLE `ProductPrice` (
  `id` int NOT NULL AUTO_INCREMENT,
  `productUnit` varchar(512) NULL,
  `unitType` varchar(512)  NULL,
  `unitPrice`  varchar(512)  NULL,
  `sellingPrice`  varchar(512)  NULL,
  `productsId` int DEFAULT NULL,
  `createdAt` varchar(512) DEFAULT NULL,
  `updatedAt` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
);


CREATE TABLE `ProductTags` (
  `id` int NOT NULL AUTO_INCREMENT,
  `tag` varchar(512) NULL,
  `productsId` int DEFAULT NULL,
  `isActive` tinyint(1) DEFAULT true,
  `isDeleted` tinyint(1) DEFAULT false,
  `createdAt` varchar(512) DEFAULT NULL,
  `updatedAt` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
);



CREATE TABLE `StripePayment` (
  `id` int NOT NULL AUTO_INCREMENT,
  `vendorsId` int DEFAULT NULL,
  `userId` int DEFAULT NULL,
  `setupUrl` varchar(512) NULL,
  `accountId` varchar(512) NULL,
  `isActive` tinyint(1) DEFAULT true,
  `isDeleted` tinyint(1) DEFAULT false,
  `createdAt` varchar(512) DEFAULT NULL,
  `updatedAt` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
);


CREATE TABLE `VendorImage` (
  `id` int NOT NULL AUTO_INCREMENT,
  `bannerImageId` varchar(512) DEFAULT NULL,
  `vendorsId` varchar(512) DEFAULT NULL,
  `createdAt` varchar(512) DEFAULT NULL,
  `updatedAt` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

ALTER TABLE Vendors Add column logoImageId varchar(512) DEFAULT NULL;



CREATE TABLE `CartItems` (
  `id` int NOT NULL AUTO_INCREMENT,
  `quantity` int DEFAULT NULL, 
  `vendorsId` int DEFAULT NULL, 
  `productsId` int DEFAULT NULL,
  `userId` int DEFAULT NULL,
  `createdAt` varchar(512) DEFAULT NULL,
  `updatedAt` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
);


CREATE TABLE `ReportedVendors` (
  `id` int NOT NULL AUTO_INCREMENT,
  `message` TEXT NULL,
  `vendorsId` int DEFAULT NULL, 
  `userId` int DEFAULT NULL,
  `createdAt` varchar(512) DEFAULT NULL,
  `updatedAt` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
);



CREATE TABLE `VendorCommissionRate` (
  `id` int NOT NULL AUTO_INCREMENT,
  `transactionCharge` varchar(512) DEFAULT NULL,
  `commissionRate` varchar(512) DEFAULT NULL,
  `enableCommission` tinyint(1) DEFAULT false,
  `vendorsId` int DEFAULT NULL, 
  `createdAt` varchar(512) DEFAULT NULL,
  `updatedAt` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

ALTER TABLE Vendors Add column vendorCommissionRateId varchar(512) DEFAULT NULL;
ALTER TABLE user Add column phoneNo varchar(512) DEFAULT NULL;
ALTER TABLE Address Add column isDefault tinyint(1) DEFAULT NULL;



CREATE TABLE `PublicHolidays` (
  `id` int NOT NULL AUTO_INCREMENT,
  `startdata` varchar(512) DEFAULT NULL,
  `endDate` varchar(512) DEFAULT NULL,
  `vendorsId` int DEFAULT NULL, 
  `createdAt` varchar(512) DEFAULT NULL,
  `updatedAt` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
);


CREATE TABLE `VendorSetting` (
  `id` int NOT NULL AUTO_INCREMENT,
  `slug` varchar(512) DEFAULT NULL,
  `value` varchar(512) DEFAULT NULL,
  `vendorsId` int DEFAULT NULL, 
  `createdAt` varchar(512) DEFAULT NULL,
  `updatedAt` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
);




# 28 May 2021

ALTER TABLE Vendors Add column urlSlug varchar(512) DEFAULT NULL;
ALTER TABLE Vendors ADD UNIQUE (urlSlug);


CREATE TABLE `VendorServiceAreas` (
  `id` int NOT NULL AUTO_INCREMENT,
  `areaName` varchar(512) DEFAULT NULL,
  `vendorsId` int DEFAULT NULL, 
  `createdAt` varchar(512) DEFAULT NULL,
  `updatedAt` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `VendorIncludePostalCode` (
  `id` int NOT NULL AUTO_INCREMENT,
  `postalCode` varchar(512) DEFAULT NULL,
  `vendorsId` int DEFAULT NULL, 
  `createdAt` varchar(512) DEFAULT NULL,
  `updatedAt` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `VendorExcludePostalCode` (
  `id` int NOT NULL AUTO_INCREMENT,
  `postalCode` varchar(512) DEFAULT NULL,
  `vendorsId` int DEFAULT NULL, 
  `createdAt` varchar(512) DEFAULT NULL,
  `updatedAt` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
);


# 29 may  2021
CREATE TABLE `UserAuthCode` (
  `id` int NOT NULL AUTO_INCREMENT,
  `code` varchar(512) DEFAULT NULL,
  `token` varchar(512) DEFAULT NULL,
  `userId` int DEFAULT NULL, 
  `createdAt` varchar(512) DEFAULT NULL,
  `updatedAt` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
);


ALTER TABLE user Add column countryCode varchar(512) DEFAULT NULL;

#30 may 2021
CREATE TABLE `Reviews` (
  `id` int NOT NULL AUTO_INCREMENT,
  `rating` varchar(512) DEFAULT NULL,
  `description` varchar(512) DEFAULT NULL,
  `userId` int DEFAULT NULL, 
  `vendorsId` int DEFAULT NULL, 
  `productsId` int DEFAULT NULL, 
  `createdAt` varchar(512) DEFAULT NULL,
  `updatedAt` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

ALTER TABLE Categories Add column imagesId varchar(512) DEFAULT NULL;


ALTER TABLE user Add column stripeCustomerId varchar(512) DEFAULT NULL;
ALTER TABLE Products Add column stripeProductId varchar(512) DEFAULT NULL;
ALTER TABLE Vendors Add column stripeConnectedAccountId varchar(512) DEFAULT NULL;
ALTER TABLE ProductPrice Add column stripeProductPriceId varchar(512) DEFAULT NULL;




CREATE TABLE `AdminSetting` (
  `id` int NOT NULL AUTO_INCREMENT,
  `metaKey` varchar(512) DEFAULT NULL,
  `metaValue` varchar(512) DEFAULT NULL,
  `createdAt` varchar(512) DEFAULT NULL,
  `updatedAt` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
);


ALTER TABLE Address Add column name varchar(512) DEFAULT NULL;
ALTER TABLE Address Add column longitude varchar(512) DEFAULT NULL;
ALTER TABLE Address Add column latitude varchar(512) DEFAULT NULL;



CREATE TABLE `OrderItem` (
  `id` int NOT NULL AUTO_INCREMENT,
  `productName` varchar(512) DEFAULT NULL,
  `productImage` varchar(512) DEFAULT NULL,
  `productUnit` varchar(512) DEFAULT NULL,
  `unitType` varchar(512) DEFAULT NULL,
  `quantity` varchar(512) DEFAULT NULL,
  `totalPrice` varchar(512) DEFAULT NULL,
  `sellingPrice` varchar(512) DEFAULT NULL,
  `isReturn`  tinyint(1) DEFAULT false,
  `ordersId` varchar(512) DEFAULT NULL,
  `userId` int DEFAULT NULL, 
  `vendorsId` int DEFAULT NULL, 
  `productsId` int DEFAULT NULL, 
  `createdAt` varchar(512) DEFAULT NULL,
  `updatedAt` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
);



CREATE TABLE `Orders` (
  `id` int NOT NULL AUTO_INCREMENT,
  `orderNumber` varchar(512) DEFAULT NULL,
  `customerNote` varchar(512) DEFAULT NULL,
  `deliveryType` varchar(512) DEFAULT NULL,
  `fromTime` varchar(512) DEFAULT NULL,
  `toTime` varchar(512) DEFAULT NULL,
  `collectionLocation` varchar(512) DEFAULT NULL,
  `deliveryCollectDay` varchar(512) DEFAULT NULL,
  `totalAmount` varchar(512) DEFAULT NULL,
  `taxAmount` varchar(512) DEFAULT NULL,
  `taxRate` varchar(512) DEFAULT NULL,
  `discountRate` varchar(512) DEFAULT NULL,
  `deliveryCharges` varchar(512) DEFAULT NULL,
  `grandTotal` varchar(512) DEFAULT NULL,
  `status` varchar(512) DEFAULT NULL,
  `refundAmount` varchar(512) DEFAULT NULL,
  `refundDeliveryCharge` varchar(512) DEFAULT NULL,
  `refundRemarks` varchar(512) DEFAULT NULL,
  `orderDate` varchar(512) DEFAULT NULL,
  `userId` int DEFAULT NULL, 
  `vendorsId` int DEFAULT NULL,  
  `createdAt` varchar(512) DEFAULT NULL,
  `updatedAt` varchar(512) DEFAULT NULL,
  `paymentIntentId` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
);


CREATE TABLE `OrderAddress` (
  `id` int NOT NULL AUTO_INCREMENT,
  `contactPerson` varchar(512) DEFAULT NULL,
  `phoneNumber` varchar(512) DEFAULT NULL,
  `postalCode` varchar(512) DEFAULT NULL,
  `fullAddress` varchar(512) DEFAULT NULL,
  `addressId` int DEFAULT NULL,
  `countryId` int DEFAULT NULL,
  `stateId` int DEFAULT NULL,
  `cityId` int DEFAULT NULL,
  `userId` int DEFAULT NULL, 
  `ordersId` int DEFAULT NULL, 
  `createdAt` varchar(512) DEFAULT NULL,
  `updatedAt` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
);


CREATE TABLE `PaymentDetails` (
  `id` int NOT NULL AUTO_INCREMENT,
  `status` varchar(512) DEFAULT NULL,
  `paymentIntentId` varchar(512) DEFAULT NULL,
  `paymentObj` TEXT DEFAULT NULL,
  `ordersId` int DEFAULT NULL, 
  `userId` int DEFAULT NULL, 
  `vendorsId` int DEFAULT NULL,  
  `createdAt` varchar(512) DEFAULT NULL,
  `updatedAt` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
);



CREATE TABLE `role` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(512) DEFAULT NULL,
  `description` varchar(512) DEFAULT NULL,
  `created` varchar(512) DEFAULT NULL,
  `modified` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
);


CREATE TABLE `roleMapping` (
  `id` int NOT NULL AUTO_INCREMENT,
  `principalType` varchar(512) DEFAULT NULL,
  `principalId` varchar(512) DEFAULT NULL,
  `roleId` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

ALTER TABLE Vendors Add column cancellationPolicy TEXT DEFAULT NULL;




# 24-June-2021
CREATE TABLE `CategorieRequest` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(512) NULL,
  `type` varchar(512) NULL,
  `status` varchar(512) NULL,
  `categoriesId` int DEFAULT NULL,
  `userId` int DEFAULT NULL,
  `isActive` tinyint(1) DEFAULT true,
  `isDeleted` tinyint(1) DEFAULT false,
  `createdAt` varchar(512) DEFAULT NULL,
  `updatedAt` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

ALTER TABLE Vendors Add column currentStatus TEXT DEFAULT NULL;

ALTER TABLE CategorieRequest Add column vendorsId int DEFAULT NULL;


ALTER TABLE Orders Add column paymentStatu varchar(512) DEFAULT NULL;

ALTER TABLE Orders Add column application_fee_amount varchar(512) DEFAULT NULL;
ALTER TABLE Orders Add column deliveryStatus varchar(512) DEFAULT NULL;



# 4- July 2021
ALTER TABLE Vendors Add column rating varchar(512) DEFAULT NULL;

# 6 July 2021
ALTER TABLE Address Add column type varchar(512) DEFAULT 'Home';



# 6 july 2021
CREATE TABLE `BecomeVendor` (
  `id` int NOT NULL AUTO_INCREMENT,
  `firstName` varchar(512) NULL,
  `middleName` varchar(512) NULL,
  `lastName` varchar(512) NULL,
  `email` varchar(512) NULL,
  `businessName` varchar(512) NULL,
  `vendorName` varchar(512)  NULL,
  `venderMobileNo` varchar(512)  NULL,
  `designation` varchar(512)  NULL,
  `RegistrationNo` varchar(512)  NULL,
  `businessAddress` TEXT  NULL,
  `businessGoogleAddress` TEXT  NULL,
  `postalCode` varchar(512)  NULL,
  `aboutUs` TEXT  NULL,
  `storeName` varchar(512)  NULL,
  `storeAddressType` varchar(512)  NULL,
  `storeAddress` TEXT  NULL,
  `countryId` int DEFAULT NULL,
  `stateId` int DEFAULT NULL,
  `cityId` int DEFAULT NULL,
  `categoriesId` int DEFAULT NULL,
  `latitude` varchar(255),
  `longitude` varchar(255),
  `vendorsId` int DEFAULT NULL,
  `vendorDesignation` varchar(512)  NULL,
  `currentStatus` varchar(512)  NULL,
  `isActive` tinyint(1) DEFAULT true,
  `isDeleted` tinyint(1) DEFAULT false,
  `createdAt` varchar(512) DEFAULT NULL,
  `updatedAt` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
);


CREATE TABLE `ResetPassword` (
  `id` int NOT NULL AUTO_INCREMENT,
  `resetPasswordToken` varchar(512) NOT NULL,
  `emailId` varchar(512) NOT NULL,
  `createdAt` varchar(512) DEFAULT NULL,
  `userId` int DEFAULT NULL,
  PRIMARY KEY (`id`)
);


ALTER TABLE AdminSetting MODIFY metaValue Text null;


CREATE TABLE `ContactUs` (
  `id` int NOT NULL AUTO_INCREMENT,
  `firstName` varchar(512) NOT NULL,
  `lastName` varchar(512) NOT NULL,
  `email` varchar(512) DEFAULT NULL,
  `phoneNo` varchar(512) DEFAULT NULL,
  `massage` varchar(512) DEFAULT NULL,
  `createdAt` varchar(512) DEFAULT NULL,
  `updatedAt` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
);


CREATE TABLE `EmailSubscribe` (
  `id` int NOT NULL AUTO_INCREMENT,
  `email` varchar(512) DEFAULT NULL,
  `createdAt` varchar(512) DEFAULT NULL,
  `updatedAt` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
);


ALTER TABLE ProductPrice Add column vendorsId varchar(512) DEFAULT NULL;


#13 july 2021

ALTER TABLE country Add column isActive tinyint(1) DEFAULT true;
ALTER TABLE country Add column isDeleted tinyint(1) DEFAULT false;

ALTER TABLE city Add column isActive tinyint(1) DEFAULT true;
ALTER TABLE city Add column isDeleted tinyint(1) DEFAULT false;

ALTER TABLE state Add column isActive tinyint(1) DEFAULT true;
ALTER TABLE state Add column isDeleted tinyint(1) DEFAULT false;



update country set isActive=false, isDeleted=true;
update country set isActive=true, isDeleted=false where id=74;

update state set isActive=false, isDeleted=true;
update state set isActive=true, isDeleted=false where countryId=74;

update city set isActive=false, isDeleted=true;
update city set isActive=true, isDeleted=false where countryId=74;


ALTER TABLE Vendors Add column stripeConnected tinyint(1) DEFAULT false;


#17 july 2021
ALTER TABLE Products Add column totalOrderQuentitySale varchar(512) DEFAULT 0;


#5 August 2021
ALTER TABLE BecomeVendor Add column userId varchar(512) DEFAULT NULL;
ALTER TABLE user Add column isVendorRequestSubmitted tinyint(1) DEFAULT false;
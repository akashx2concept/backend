var http = require('https');
const globalConfig = require('../globalConfig');
const mailTemplate = require ('./mailTemplate');



exports.sendMail_usingSendgrid = async (dataObj) => {

    console.log(dataObj);

    let templateData = '<h1>Demo Template</h1>'
    if (dataObj['mailType']) {
      mailType = dataObj['mailType'];
      templateData = await method_for_finding_Tempalte_toSend (mailType, dataObj);
    } else {
      return false;
    }
    

    const msg1 = {
        to: dataObj.email,
        userName: dataObj.fullName,
        from: globalConfig.sendGridSendMailEmailId,
        subject: dataObj.subject,
        text: templateData,
        html: templateData,
      };
      // console.log(msg1);
      let options = {
        method: 'POST',
        host: 'api.sendgrid.com',
        path: 'https://api.sendgrid.com/v3/mail/send',
        headers: {
          'Content-Type': 'application/json',
          Authorization: globalConfig.sendGridAutherizationToken,
        },
      };
  
      let req = http.request(options, function (res) {
        let chunks = [];
        res.on('data', function (chunk) {
          chunks.push(chunk);
        });
        res.on('end', function () {
          let body = Buffer.concat(chunks);
          body = body.toString();
          console.log(body, "!!!!!!!!!");
          return true;
        //   doRes.status(200).send({
        //     message: "Mail Send"
        //   });
        });
      });
      let fdata = {
        personalizations: [{
          to: [{
            email: dataObj.email,
            name: msg1.userName
          },],
        },],
        from: {
          email: globalConfig.sendGridSendMailEmailId,
          name: globalConfig.sendGridSendMailUserName
        },
        subject: dataObj.subject,
        content: [{
          type: 'text/plain',
          value: templateData,
        },
        {
          type: 'text/html',
          value: templateData,
        },
        ],
      };
  
      req.write(JSON.stringify(fdata));
      req.end();
  




}




async function method_for_finding_Tempalte_toSend (mailType, data) {

  let template = "<h1>Demo</h1>";

  if (mailType == 'VerificationMail') {
    template = await mailTemplate.verificationMail (data);
  } else if (mailType == '2WayAuthenticationMail') {
    template = await mailTemplate.sendOTP_onMail(data);
  } else if (mailType == 'ForgotPassword') {
    console.log("linee checked")
    template = await mailTemplate.forgotPasswordMail(data);
  } else if (mailType == "VendorApproved") {
    template = await mailTemplate.VenndorApprovedMail(data);
  }


  return template;


}
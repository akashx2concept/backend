
let countryJSON = require('./country');
let stateJSON  = require('./state');
let cityJSON = require('./city');


module.exports = function (app) {  



    app.post("/api/createCountry", async function (doreq, dores) {
        createCountry(doreq, dores);
    });

    async function createCountry (doreq, dores) {
        var countryList = await countryJSON.countryJson();
        let countryRepository = app.models.country;
        for (let a=0; a<=countryList.length-1; a++) {
            let data = {
                "id": countryList[a]['id'],
                "name": countryList[a]['name'],
                "phone_code": countryList[a]['phone_code'],
                "currency":  countryList[a]['currency'],
                "currency_symbol": countryList[a]['currency_symbol'],
                "latitude": countryList[a]['latitude'],
                "longitude": countryList[a]['longitude'],
                "emoji": countryList[a]['emoji'],
                "emojiU": countryList[a]['emojiU'],
            }
            var createdData = await countryRepository.create(data);
            console.log(createdData.id, "country");
        }
        dores.status(200).send({massage: "Successfully created"})
    }



    
    app.post("/api/createState", async function (doreq, dores) {
        createState(doreq, dores);
    });

    async function createState (doreq, dores) {
        var stateList = await stateJSON.stateJSON();
        let stateRepository = app.models.state;
        for (let a=0; a<=stateList.length-1; a++) {
            let data = {
                "id": stateList[a]['id'],
                "name": stateList[a]['name'],
                "country_code": stateList[a]['country_code'],
                "state_code": stateList[a]['state_code'],
                "latitude": stateList[a]['latitude'],
                "longitude": stateList[a]['longitude'],
                "countryId": stateList[a]['country_id'],
            }
            var createdData = await stateRepository.create(data);
            console.log(createdData.id, "state");
        }
        dores.status(200).send({massage: "Successfully created"})
    }


    app.post("/api/createCity", async function (doreq, dores) {
        createCity(doreq, dores);
    });

    async function createCity (doreq, dores) {
        var cityList = await cityJSON.cityJSON();
        let cityRepository = app.models.city;
        for (let a=0; a<=cityList.length-1; a++) {
            let data = {
                "id": cityList[a]['id'],
                "name": cityList[a]['name'],
                "state_code": cityList[a]['state_code'],
                "country_code": cityList[a]['country_code'],
                "latitude": cityList[a]['latitude'],
                "longitude": cityList[a]['longitude'],
                "countryId": cityList[a]['country_id'],
                "stateId": cityList[a]['state_id'],
            }
            var createdData = await cityRepository.create(data);
            console.log(createdData.id, "city");
        }
        dores.status(200).send({massage: "Successfully created"})
    }


}
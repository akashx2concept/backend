// Copyright IBM Corp. 2016,2019. All Rights Reserved.
// Node module: loopback-workspace
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT

'use strict';

const loopback = require('loopback');
const boot = require('loopback-boot');
var bodyParser = require('body-parser');

var customApi = require('./api/index');

const app = module.exports = loopback();


app.use(bodyParser.urlencoded());

app.use(bodyParser.json());

var path = require('path');
app.use('/static', loopback.static(path.resolve(__dirname, '../images')))
// app.use('/static1', loopback.static(path.resolve(__dirname, '../document')))

app.use(loopback.static(path.resolve(__dirname, '../client')));


const fs = require("fs");
app.use('*', (req, res, next) => {
  if (req.baseUrl.search("/api") != -1) {
    // console.log(req.baseUrl)
    next();
  } else if (req.baseUrl.search("/explorer") != -1) {
    next();
  } else {
    let rs = fs.createReadStream(`${__dirname}/../client/index.html`);
    res.writeHead(200, {
      "Content-type": "text/html"
    });
    rs.pipe(res);
  }
})


app.start = function () {
  // start the web server
  return app.listen(function () {
    app.emit('started');
    const baseUrl = app.get('url').replace(/\/$/, '');
    console.log('Web server listening at: %s', baseUrl);
    if (app.get('loopback-component-explorer')) {
      const explorerPath = app.get('loopback-component-explorer').mountPath;
      console.log('Browse your REST API at %s%s', baseUrl, explorerPath);
    }
  });
};


// const fs = require("fs");
// app.get((req, res) => {
//   console.log(req, "sdgdf");
//   let rs = fs.createReadStream(`${__dirname}/../client/index.html`);
//   res.writeHead(200, {
//     "Content-type": "text/html"
//   });
//   rs.pipe(res);
// })

// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, function (err) {
  if (err) throw err;

  // start the server if `$ node server.js`
  // if (require.main === module)
  // app.start();


  var serverStartedTime = (new Date).getTime();
  app.io = require('socket.io')(app.start());
  app.io.on('connection', function (socket) {
    app.io.emit('Socket Init', {
      startTime: serverStartedTime
    });
  });


  setTimeout(() => {
    new customApi(app);
  })
});



// // Set your secret key. Remember to switch to your live secret key in production!
// // See your keys here: https://dashboard.stripe.com/apikeys
// const Stripe = require('stripe');
// const stripe = Stripe('sk_test_51IdCiXE1e1bineRZvMq6a96aZpha2AONtWmyiBXvoFpAaTkvWYyxsrK7vjLZpS217iKTbyGk6WKdgmoIE1yDAWNM002rWDaE6M');

// // If you are testing your webhook locally with the Stripe CLI you
// // can find the endpoint's secret by running `stripe listen`
// // Otherwise, find your endpoint's secret in your webhook settings in the Developer Dashboard
// const endpointSecret = 'whsec_CXs9UnIot9kBu9MLofLrudnHwieK9hYU';

// app.post('/webhook', bodyParser.raw({type: 'application/json'}), (request, response) => {
//   console.log("%%%%%%%%%%%%%%%%%%%%%%%%% stripe");
//   const sig = request.headers['stripe-signature'];

//   console.log(sig)

//   let event;

//   // Verify webhook signature and extract the event.
//   // See https://stripe.com/docs/webhooks/signatures for more information.
//   try {
//     event = stripe.webhooks.constructEvent(request.body, sig, endpointSecret);
//   } catch (err) {
//     return response.status(400).send(`Webhook Error: ${err.message}`);
//   }

//   if (event.type === 'checkout.session.completed') {
//     const session = event.data.object;
//     handleCompletedCheckoutSession(session);
//   }

//   response.json({received: true});
// });

// const handleCompletedCheckoutSession = (session) => {
//   // Fulfill the purchase.
//   console.log(JSON.stringify(session));
// }
